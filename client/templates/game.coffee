
@noSleep = null

Template.game.rendered = ->
  noSleep = new NoSleep()
  noSleep.enable()

Template.game.destroyed = ->
  if noSleep
    noSleep.disable()

Template.game.helpers
  people: ->
    People.find(user_id: Meteor.userId())

  isPaused: ->
    Session.get 'isPause'

  isActivePlayer: ->
    timer = GameTimer.findOne(user_id: Meteor.userId())
    if timer?.activePerson?
      timer.activePerson is @_id

  isNextPlayer: ->
    getNextPlayer() is @_id

  getRemainingTime: ->
    timer = GameTimer.findOne(user_id: Meteor.userId())
    if timer?.activePerson? and timer.activePerson is @_id
      secondsToHms(Session.get("time"), timer.timerPerTurn >= 3600)

  getRemainingGlobalTime: ->
    timer = GameTimer.findOne(user_id: Meteor.userId())
    if timer?.activePerson? and timer.activePerson is @_id
      s = @timer.timerPerOverall - (@timer.timerPerTurn - Session.get "time")
      if s <= 0
        if Template.game.interval
          Meteor.clearInterval Template.game.interval
        if timer?.activePerson
          People.update timer.activePerson, $set: 'timer.timerPerOverall': 0
          Session.set "time", 0
        Router.go 'game.finsh'

      secondsToHms(s, timer.timerPerOverall >= 3600)
    else
      secondsToHms(@timer.timerPerOverall, @timer.timerPerOverall >= 3600)


getNextPlayer = () ->
  console.log 'make turn'
  gs = GameTimer.findOne(user_id: Meteor.userId())
  goombasIds = People.find(user_id: Meteor.userId()).map( (s) -> s._id)
  if not gs.activePerson
    goombasIds[0]
  else
    #ищем индекс в массиве
    index = _.indexOf(goombasIds, gs.activePerson)
    index += 1
    if index != goombasIds.length
      goombasIds[index]
    else
      #все походили - перееходим на следующий ход
      goombasIds[0]


Template.game.events
  'click .pause' : (e,template) ->
    Session.set('isPause', not Session.get('isPause'))

  'click .next-player' : (e,template) ->
    $("##{getNextPlayer()}").trigger('click')

  'click .abort-game' : (e,template) ->
    timer = GameTimer.findOne(user_id: Meteor.userId())

    if Template.game.interval
      Meteor.clearInterval Template.game.interval

    if timer?.activePerson
      prevPlayer = People.findOne(timer.activePerson)
      People.update timer.activePerson, $set: 'timer.timerPerOverall': prevPlayer.timer.timerPerOverall - (prevPlayer.timer.timerPerTurn - Session.get "time")
      Session.set "time", 0
    Router.go 'game.finsh'

  'click .player' : (e,template) ->
    if getNextPlayer() is @_id
      timer = GameTimer.findOne(user_id: Meteor.userId())
      console.log 'next'

      #сбрасываем предыдущего игрока
      if Template.game.interval
        Meteor.clearInterval Template.game.interval

      if timer?.activePerson
        prevPlayer = People.findOne(timer.activePerson)
        People.update timer.activePerson, $set: 'timer.timerPerOverall': prevPlayer.timer.timerPerOverall - (prevPlayer.timer.timerPerTurn - Session.get "time")
        Session.set "time", @timer.timerPerTurn

      #устанавливаем нового

      GameTimer.update timer._id, $set: activePerson: @_id

      clock = @timer.timerPerTurn

      timeLeft = ->
        unless Session.get('isPause')
          if clock > 0
            clock--
            Session.set "time", clock
            console.log clock
          else
            console.log "That's All Folks"
            Meteor.clearInterval Template.game.interval

      Template.game.interval = Meteor.setInterval(timeLeft, 1000)
#      console.log Template.game.interval
    else
      console.log 'not your turn!'
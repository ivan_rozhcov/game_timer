Template.createUsers.helpers
  people: ->
    People.find(user_id: Meteor.userId())
  gameTimer: ->
    GameTimer.findOne(user_id: Meteor.userId())

AutoForm.addHooks 'afTimerSettings',
  onSuccess: (insertDoc, updateDoc, currentDoc) ->
    timer = GameTimer.findOne(user_id: Meteor.userId())
    for pep in People.find(user_id: Meteor.userId()).fetch()
      People.update pep._id, $set: timer: timer
    GameTimer.update timer._id, $set: activePerson: null
    Router.go 'game.game'

  formToModifier: (doc) ->
    console.log doc['$set']
    if doc['$set']
#              if @value >= convertToSeconds(AutoForm.getFieldValue('timerPerOverall'))
      timerPerTurn = convertToSeconds doc.$set.timerPerTurn
      timerPerOverall = convertToSeconds doc.$set.timerPerOverall
      if timerPerTurn >= timerPerOverall
        GameSettingsSchema.namedContext('afTimerSettings').addInvalidKeys [ {
          name: 'timerPerTurn'
          type: 'turnTimeGreaterThanOverall'
        } ]
        return
      doc.$set.timerPerTurn = timerPerTurn
      doc.$set.timerPerOverall = timerPerOverall
      console.log doc['$set']
    doc

  docToForm: (doc) ->
    if doc.timerPerTurn
      doc.timerPerTurn = moment().startOf('day').seconds(doc.timerPerTurn).format('HH:mm:ss')
    if doc.timerPerOverall
      doc.timerPerOverall = moment().startOf('day').seconds(doc.timerPerOverall).format('HH:mm:ss')
    doc
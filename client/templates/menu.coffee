Template.menu.events
  'click .create-game' : (e,t) ->
    Router.go 'game.create'
    e.stopPropagation()
  'click .join-game' : (e,t) ->
    Router.go 'game.join_game'
  'click .go-score' : (e,t) ->
    Router.go 'game.score'
  'click .sign_out' : (e,t) ->
    Meteor.logout()
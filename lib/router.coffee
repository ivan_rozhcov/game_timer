Router.configure
  layoutTemplate: 'Layout'
  notFoundTemplate: 'notFound'
  loadingTemplate: 'loading'
  onBeforeAction: ->
    userId = if Meteor.isClient then Meteor.userId() else @userId
    if userId
      @next()
    else
      if Meteor.isClient then @render('login') else @next()


Router.route '/',
  template: 'menu'
  name: 'game.menu'
  onBeforeAction: ->
    if Meteor.userId()
      @next()
    else
      @render 'login'

Router.route '/create',
  template: 'createUsers'
  name: 'game.create'
  onBeforeAction: ->
    if Meteor.userId()
      @next()
    else
      @render 'login'


Router.route '/game',
  template: 'game'
  name: 'game.game'
  onBeforeAction: ->
    if Meteor.userId()
      @next()
    else
      @render 'login'

Router.route '/finish',
  template: 'finish'
  name: 'game.finsh'
  onBeforeAction: ->
    if Meteor.userId()
      @next()
    else
      @render 'login'
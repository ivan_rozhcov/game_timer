@convertToSeconds = (hms) ->
  # your input string
  unless hms
    return 0
  a = hms.split(':')
  if a.length is 2
    a[0] * 60 * 60 + +a[1] * 60
  else
    # split it at the colons
    # minutes are worth 60 seconds. Hours are worth 60 minutes.
    a[0] * 60 * 60 + +a[1] * 60 + +a[2]

@secondsToHms = (seconds, withHours) ->
  moment().startOf('day').seconds(seconds).format(if withHours then 'HH:mm:ss' else 'mm:ss')
#!/bin/bash
set -e

### Configuration ###

APP=gametimer
#USER=$APP
USER=goldfish
SUPER_USER=ivan133
DOMAIN=gametimer.ivan133.ru
SERVER=${USER}@${DOMAIN}
SERVER_SUDO=${SUPER_USER}@${DOMAIN}
APP_DIR=/var/www/${APP}
DB_NAME=${APP}_db
KEYFILE=
REMOTE_SCRIPT_PATH=/home/${USER}/deploy-${APP}.sh
RESTART_ARGS=
NGINX_CONF_PATH=/etc/nginx/sites-enabled/${APP}.conf

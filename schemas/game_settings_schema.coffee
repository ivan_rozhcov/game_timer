@GameSettingsSchema = new SimpleSchema
  user_id:
    type: String
  timerPerTurn:
    type: Number
    label: 'timer Per Turn (hh:mm:ss)'
    min: 1
    autoform:
      afFieldInput:
        type: "time"
        step: 1
    custom: ->
      console.log @value, convertToSeconds(AutoForm.getFieldValue('timerPerOverall'))
      if @value >= convertToSeconds(AutoForm.getFieldValue('timerPerOverall'))
          GameSettingsSchema.namedContext('afTimerSettings').addInvalidKeys [ {
            name: 'timerPerTurn'
            type: 'turnTimeGreaterThanOverall'
          } ]
      return
  timerPerOverall:
    type: Number
    label: 'timer Per Overall (hh:mm:ss)'
    min: 1
    autoform:
      afFieldInput:
        type: "time"
        step: 1